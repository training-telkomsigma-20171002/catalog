package id.artivisi.training.telkomsigma.marketplace.catalog.controller;

import id.artivisi.training.telkomsigma.marketplace.catalog.dao.ProductDao;
import id.artivisi.training.telkomsigma.marketplace.catalog.dao.ProductPhotosDao;
import id.artivisi.training.telkomsigma.marketplace.catalog.entity.Product;
import id.artivisi.training.telkomsigma.marketplace.catalog.entity.ProductPhotos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController @RequestMapping("/api/product")
public class ProductApiController {
    @Autowired private ProductDao productDao;
    @Autowired private ProductPhotosDao productPhotosDao;

    @GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product p){
        //Product p = productDao.findOne(id); // tidak perlu lagi, karena sudah disediakan Spring Data JPA
        return p;
    }

    @GetMapping("/{id}/photos")
    public Iterable<ProductPhotos> findPhotosForProduct(@PathVariable("id") Product p){
        return productPhotosDao.findByProduct(p);
    }
}
